#include <SDL.h>
#include <stdbool.h>
#include <stdio.h>
#include <math.h>
#include <getopt.h>
#include "julia.h"

#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#ifndef MAXFPS
#define MAXFPS 60
#endif

int W = 640;
int H = 480;
Uint32 ticks = 0;
bool redraw_flag = false;
bool regen_flag = false;
bool rightclick = false;
struct {
	bool started;
	struct {
		int x, y;
	} start, end;
} selection;
struct {
	double x1, y1, x2, y2;
} fractal_bounds = {-R, -R, R, R};
typedef struct {
	double x, y;
} Point;
SDL_Texture *fractal_texture;

Point
CanvasToFractalCoords(Point canvas_coords)
{
	int size = MIN(W, H);
	return (Point) {
		.x = fractal_bounds.x1 + canvas_coords.x * (fractal_bounds.x2 - fractal_bounds.x1) / size,
		.y = fractal_bounds.y1 + canvas_coords.y * (fractal_bounds.y2 - fractal_bounds.y1) / size};
}

int
GetCanvasSize(void)
{
	return MIN(W, H);
}

SDL_Point
GetCanvasPadding(int size)
{
	return (SDL_Point) {.x = (W - size)/2, .y = (H - size)/2};
}

void
ClearRenderer(SDL_Renderer *renderer)
{
	SDL_SetRenderDrawColor(renderer, 32, 32, 96, 255);
	SDL_RenderClear(renderer);
}

void
CropFractalToSelection(void)
{
	int size = GetCanvasSize();
	SDL_Point padding = GetCanvasPadding(size);

	Point canvas_sel_start = {
		.x = selection.start.x - padding.x,
		.y = selection.start.y - padding.y
	};
	Point canvas_sel_end = {
		.x = selection.end.x - padding.x,
		.y = selection.end.y - padding.y
	};

	Point fractal_sel_start = CanvasToFractalCoords(canvas_sel_start);
	Point fractal_sel_end = CanvasToFractalCoords(canvas_sel_end);

	fractal_bounds.x1 = fractal_sel_start.x;
	fractal_bounds.y1 = fractal_sel_start.y;
	fractal_bounds.x2 = fractal_sel_end.x;
	fractal_bounds.y2 = fractal_sel_end.y;
}

void
GenerateFractal(SDL_Renderer *renderer)
{
	SDL_Log("fractal_texture regeneration\n");
	int size = GetCanvasSize();

	if (fractal_texture != NULL) {
		SDL_DestroyTexture(fractal_texture);
		fractal_texture = NULL;
	}
	fractal_texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, size, size);
	SDL_SetRenderTarget(renderer, fractal_texture);
	ClearRenderer(renderer);

	Point canvas_coords, fractal_coords;
	for (canvas_coords.x = 0; canvas_coords.x < size; ++canvas_coords.x)	{
		for (canvas_coords.y = 0; canvas_coords.y < size; ++canvas_coords.y)	{
			fractal_coords = CanvasToFractalCoords(canvas_coords);

			int c = GetPointIterations(fractal_coords.x, fractal_coords.y);

			SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255.0 * c / maxiter);
			SDL_RenderDrawPoint(renderer, canvas_coords.x, canvas_coords.y);
		}
	}

	SDL_SetRenderTarget(renderer, NULL);
}

void
DrawFractal(SDL_Renderer *renderer)
{
	if (fractal_texture == NULL) {
		SDL_Log("No fractal_texture!\n");
		return;
	}

	int size;
	SDL_QueryTexture(fractal_texture, NULL, NULL, &size, NULL);
	SDL_Point padding = GetCanvasPadding(size);

	SDL_RenderCopy(renderer, fractal_texture, NULL, &(SDL_Rect) {
			.x = padding.x, .y = padding.y, 
			.w = size, .h = size
	});
}

void
DrawSelectionRectangle(SDL_Renderer *renderer)
{
	if (selection.started)
	{
		SDL_SetRenderDrawColor(renderer, 255, 128, 128, 255);
		int w = selection.end.x - selection.start.x;
		int h = selection.end.y - selection.start.y;
		SDL_RenderDrawRect(renderer, &(SDL_Rect) {
				.x = selection.start.x,
				.y = selection.start.y,
				.w = w, .h = h
				});
	}
}

void
PrintPointInformation(int x, int y)
{
	int size = GetCanvasSize();
	SDL_Point padding = GetCanvasPadding(size);

	Point canvas_coords = {
		.x = x - padding.x,
		.y = y - padding.y
	};

	Point fractal_coords = CanvasToFractalCoords(canvas_coords);

	int c = GetPointIterations(fractal_coords.x, fractal_coords.y);

	SDL_Log("x=%d y=%d fractal_coords.x=%lf fractal_coords.y=%lf c=%d\n", 
			x, y, fractal_coords.x, fractal_coords.y, c);
}


bool
HandleEvent(SDL_Event event)
{
	bool quit = false;
	SDL_Keymod mod;
	switch (event.type) {
		case SDL_QUIT:
			quit = true;
			break;
		case SDL_KEYDOWN:
			mod = SDL_GetModState();
			switch (event.key.keysym.sym)	{
				case SDLK_q:
					if (mod & KMOD_LCTRL || mod & KMOD_RCTRL) 
						quit = true;
					break;
				case SDLK_ESCAPE:
					if (selection.started) {
						selection.started = false;
						redraw_flag = true;
					}
					break;
			}
			break;
		case SDL_MOUSEBUTTONDOWN:
			switch (event.button.button) {
				case SDL_BUTTON_RIGHT:
					rightclick = true;
				break;
				case SDL_BUTTON_LEFT:
					selection.started = true;
					selection.start.x = event.button.x;
					selection.start.y = event.button.y;
				break;

			}
			break;
		case SDL_MOUSEBUTTONUP:
			switch (event.button.button) {
				case SDL_BUTTON_RIGHT:
					if (rightclick) {
						PrintPointInformation(event.button.x, event.button.y);
						rightclick = false;
					}
					break;
				case SDL_BUTTON_LEFT:
					if (selection.started) {
						selection.started = false;
						selection.end.x = event.button.x;
						selection.end.y = event.button.y;
						SDL_Log("region selected from (%d, %d) to (%d, %d)\n",
						selection.start.x, selection.start.y,
						selection.end.x, selection.end.y);
						CropFractalToSelection();
						regen_flag = true;
						redraw_flag = true;
					}
					break;
			}
			break;
		case SDL_MOUSEMOTION:
			if (selection.started) {
				selection.end.x = event.button.x;
				selection.end.y = event.button.y;
				redraw_flag = true;
			}
			break;
		case SDL_WINDOWEVENT:
			switch (event.window.event) {
				case SDL_WINDOWEVENT_EXPOSED:
					regen_flag = true;
					redraw_flag = true;
					break;
			}
			break;
	}
	return quit;
}

Uint32
getDelta(void)
{
	Uint32 current = SDL_GetTicks();
	Uint32 delta = current - ticks;
	ticks = current;
	if(delta < 1000/MAXFPS) SDL_Delay(1000/MAXFPS - delta);
	return delta;
}

void
Redraw(SDL_Renderer *renderer)
{
	//SDL_Log("redraw");
	ClearRenderer(renderer);

	DrawFractal(renderer);
	DrawSelectionRectangle(renderer);
	SDL_RenderPresent(renderer);
}

void
RunMainLoop(SDL_Window *window, SDL_Renderer *renderer)
{
	bool quit = false;
	Uint32 delta = 0;
	while (!quit) {
		SDL_GetWindowSize(window, &W, &H);
		SDL_Event event;
		SDL_PollEvent(&event);
		quit = HandleEvent(event);

		if (regen_flag) {
			GenerateFractal(renderer);
			regen_flag = false;
		}
		if (redraw_flag) {
			Redraw(renderer);
			redraw_flag = false;
		}
		
		delta = getDelta();
	}
}

void
PrintUsage(char *argv0)
{
	printf("usage: %s [-h] [-n iterations]\n", argv0);
}

void
ParseOptions(int argc, char **argv)
{
	int opt;
	while ((opt = getopt(argc, argv, "n:h")) != -1) {
		switch (opt) {
			case 'n':
				maxiter = atoi(optarg);
				break;
			case 'h':
				PrintUsage(argv[0]);
				exit(0);
				break;
			default:
				PrintUsage(argv[0]);
				exit(EXIT_FAILURE);
		}
	}
}

int
main(int argc, char **argv)
{
	ParseOptions(argc, argv);

	SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO);

	SDL_Window *window;
	window = SDL_CreateWindow(
			"Fractal Zoomer",
			SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED,
			W,
			H,
			SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE
			);
	SDL_Renderer * renderer = SDL_CreateRenderer(window, -1, 0);
	SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

	regen_flag = true;
	redraw_flag = true;
	RunMainLoop(window, renderer);

	if (fractal_texture == NULL)
		free(fractal_texture);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
}
