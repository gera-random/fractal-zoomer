#include <complex.h>
#include "julia.h"

int maxiter = 1000;

int
GetPointIterations(double x, double y)
{
	double complex z = x + y * I;

	int iter = 0;
	while (cabs(z) < R * R && iter < maxiter) {
		z = z * z + C;
		++iter;
	}

	return iter;
}
