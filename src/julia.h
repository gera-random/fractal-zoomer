#pragma once

#ifndef R
#define R 3.5
#endif
#ifndef C
#define C -0.1 + 0.65 * I
#endif

extern int maxiter;

/** @brief Get number of iterations for point x, y in the Julia set
 *
 *  @param x point x coordinate scaled between -R and R
 *  @param y point y coordinate scaled between -R and R
 *  @return number of iterations between 0 and MAXITER
 */
int GetPointIterations(double x, double y);
